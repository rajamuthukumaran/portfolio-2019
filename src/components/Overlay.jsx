import React from 'react';
import vid_bg from '../assets/vid/bg.mp4';
import pstr_img from '../assets/img/posterimage.png';
import '../scss/components/Overlay.scss';

const Overlay = () => {
    return (
        <>
            <div className="vid-overlay1"></div>
                <div className="fullscreen-video-wrap">
                <video autoPlay loop poster={pstr_img} muted>
                    <source src={vid_bg} type="video/mp4" />
                </video>
            </div>
        </>
     );
}
 
export default Overlay;