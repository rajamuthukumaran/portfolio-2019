import React, { Component } from 'react';
import Button from '../Button';
import '../../scss/components/Panel.scss';

class Panel extends Component {
    
    show(stat){
        if(stat){
            return ""
        }
        return "hide";
    }

    render(props) {
        const { name, children, onPnlClick, enable, size, type, id } = this.props;

        const pnl_style = {
            height: size+"vh"
        }
        return ( 
            <div style={pnl_style} className={"panel "+name+" "+type+" "+this.show(enable)}>
                <Button name="pnl_clse" onBtnClick={onPnlClick} id={id} size={15} btn_img={require('../../assets/img/close.svg')}/>
                {children}
            </div>
         );
    }
}
 
export default Panel;