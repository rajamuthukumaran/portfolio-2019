import React, { Component } from 'react';
import Button from '../Button';
import RMK from "../../assets/img/rmk.jpg";
import '../../scss/components/Details.scss';

class Details extends Component {
  state = { 
    name: "Rajamuthukumaran D",
    dob: new Date(1996,11,9,9,30),
    age: 22,
    loc: 'India'
   }
   
  componentDidMount(){
    const dob = this.state.dob;
    const tdy = Date.now();
    const age = Math.floor((tdy - dob)/31556952000);
    this.setState({
      age
    })
  }

  gitSite(){
    window.open("gitlab.com/rajamuthukumaran",'_blank');
  }

  render() { 
    const { name, age, loc } = this.state;
    const { onPnlClick, enable, id, onBtnClick } = this.props;
    return (
      <>
        <div className={enable? "details " : "hide"}>
          <Button name="dtl_clse" onBtnClick={onPnlClick} id={id} size={15} btn_img={require('../../assets/img/close.svg')}/>
          <div className="content">
            <img src={RMK} alt="Rajamuthukumaran D" onClick={this.gitSite}/>
            <div className="det">
                <p><span>Name : </span>{name}</p>
                <p><span>Age : </span>{age}</p>
                <p><span>Gender : </span>Male</p>
                <p><span>Location : </span>{loc}</p>
            </div>
          </div>
        </div>
        <div className={!enable? "dtl_btn" : "hide"} id={id} onClick={onBtnClick}></div>
      </>
    );
  }
}
 
export default Details;
