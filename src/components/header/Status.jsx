import React, { Component } from 'react';
import '../../scss/components/Status.scss';

class Status extends Component {
    state = { 
        time: null,
        isLoaded: false,
        error: false,
        weather: [],
        climate: null
     }

    componentDidMount(){
        const API_KEY = process.env.REACT_APP_WEATHER_API_KEY;
        const API = `http://api.openweathermap.org/data/2.5/forecast?id=1264527&APPID=${API_KEY}&units=imperial`;
        setInterval(() => {
            this.setState({
                time: new Date().toLocaleTimeString('en-us',{hour12: true, timeStyle: "short"})
            })
        }, 1000);
        fetch(API)
            .then(res => res.json())
            .then(result => {
                console.log(result)
                const temp = result.list.filter(i => i.dt >= (this.state.time/1000))[0];
                const code = result.cod;
                if(code == 200){
                    const climate = temp.weather[0];
                    this.setState({
                        isLoaded: true,
                        error: false,
                        weather: temp.main,
                        climate: climate.icon
                    })
                }else{
                    this.setState({
                        isLoaded: true,
                        error: true
                    })
                }
            });
    }

    getWeather(){
        const { isLoaded, error, weather } = this.state;
        if(!error){
            if(isLoaded){
                const temp = weather.temp;
                return temp+"°F"
            }
            return '...'
        }
        return ''
    }

    render() { 
        const wthr_icon = this.state.climate;
        return ( 
            <div className="status">
                <div className="time">
                    <p>{this.state.time}</p>
                </div>
                <div className="weather">
                    <img src={`http://openweathermap.org/img/wn/${wthr_icon}@2x.png`} alt=""/>
                    <p>{this.getWeather()}</p>
                </div>
            </div>
         );
    }
}
 
export default Status;