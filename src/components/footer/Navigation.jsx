import React, { Component } from 'react';
import Button from '../Button';
import '../../scss/components/Navigation.scss'

class Navigation extends Component {
    state = {  }

    navButtons(){
        console.log(this.props)
        const buttons = this.props.data.map( i => {
            return (

                <Button name={"nav_btn nav_"+i.name} size={42} id={i.id} key={i.id} onBtnClick={this.props.onBtnClick}/>
            )
        })
        return buttons;
    }

    navIcon(stat){
        if(stat){
            return require('../../assets/img/close.svg');
        }
        return require('../../assets/img/menu.svg');
    }

    render() { 
        const { onNavClick, enable } = this.props;
        return ( 
            <nav className="nav">
                <div className={!enable && 'hide'}>
                    {this.navButtons()}
                </div>
                <Button name="nav_menu" onBtnClick={onNavClick} size={30} btn_img={this.navIcon(enable)}/>
                <div className={enable? "nav_overlay": "hide"}></div>
            </nav>
         );
    }
}
 
export default Navigation;