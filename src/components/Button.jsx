import React from 'react';
import '../scss/components/Button.scss'

const Button = (props) => {

    const { size, onBtnClick, name, btn_img, id } = props

    const btn_sz = size/2;
    const btn_bg = `url(${btn_img}) no-repeat center /${btn_sz}px`;
    const btn_style = {
        width: size,
        height: size,
        background: btn_bg,
    }

    return ( 
        <div style={btn_style} className={"btn "+name} id={id} onClick={onBtnClick}>

        </div>
     );
}
 
export default Button;