import React, { Component } from "react";
import Panel from "./components/main/Panel";
import Details from "./components/main/Details";
import Draggable from "react-draggable";

class MainContent extends Component {
  state = {};
  render() {
    const desc_data = this.props.data.filter(i => i.id == 2)[0];
    const det_data = this.props.data.filter(i => i.id == 1)[0];
    const { onPnlClick, onBtnClick } = this.props;
    return (
      <main>
        <Details
          onPnlClick={onPnlClick}
          onBtnClick={onBtnClick}
          id={det_data.id}
          enable={det_data.enable}
        />
        <Panel
          name="desc"
          onPnlClick={onPnlClick}
          enable={desc_data.enable}
          id={desc_data.id}
        >
          <h3>Hi, nice to meet you,</h3>
          <p>
            This is <span>Rajamuthukumaran</span>. Yeah, I know that is a mouth
            full. You can call me Raja. I'm a passionate programmer who loves
            making things come alive through code. I see it as an art. I don't
            have a specific domain I'm specialized in. I do Linux, Windows,
            Android and Web apps.
          </p>
        </Panel>
      </main>
    );
  }
}

export default MainContent;
