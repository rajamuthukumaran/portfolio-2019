import React, { Component } from 'react';
import Navigation from './components/footer/Navigation';
import './scss/Footer.scss'

class Footer extends Component {
    state = {  }

    render() { 
        const year = new Date().getFullYear();
        return ( 
            <footer>
                <div className="creation">
                    <p className="copyright">@rajamuthukumaran {year}</p>
                </div>
                <Navigation data={this.props.data} onNavClick={this.props.onNavClick} onBtnClick={this.props.onBtnClick} enable={this.props.flags}/>
            </footer>
         );
    }
}
 
export default Footer;