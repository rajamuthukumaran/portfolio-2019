import React, { Component } from 'react';
import Header from './Header';
import Overlay from './components/Overlay';
import MainContent from './MainContent';
import Footer from './Footer';
import './scss/App.scss'

class App extends Component {
  constructor(props) {
    super(props);
    this.state = { 
      panels: [
        {id: 1, name: 'details', enable: true},
        {id: 2, name: 'desc', enable: true},
        {id: 3, name: 'prof', enable: true},
        {id: 4, name: 'chat', enable: true}
      ],
      nav_status: false
     }
     this.handleNavClick = this.handleNavClick.bind(this);
     this.handleNav = this.handleNav.bind(this);
     this.handleClosePnl = this.handleClosePnl.bind(this);
  }

  handleNavClick(event){
    const id = event.target.id;
    console.log(this.state.panels.filter(i => i.id == id)[0].enable)//!prevState.panels.filter(i => i.id == id)[0].enable
    this.setState(prevState => {
       const update = prevState.panels.map(i => {
          if(i.id == id){
            i.enable = true;
          }
          return i;
       });
       return {
         panels: update,
         nav_status: false
       }
    });
  }

  handleNav(event){
    // const desc_stat = this.state.panels.filter(i => i.id == 2)[0];
    // if(desc_stat.enable){
    //   document.getElementById('root').addClass("nav_blur");
    // }
    this.setState(prevState => {
      return { nav_status: !prevState.nav_status };
    });
  }

  handleClosePnl(event){
    const id = event.target.id;
    this.setState(prevState => {
       const update = prevState.panels.map(i => {
          if(i.id == id){
            i.enable = false;
          }
          return i;
       });
       return {
         panels: update
       }
    });
  }

  render() {
    const { panels, nav_status } = this.state
    return ( 
      <>
        <Overlay/>
        <Header/>
        <MainContent onPnlClick={this.handleClosePnl} onBtnClick={this.handleNavClick} data={panels} flags={nav_status}/>
        <Footer onNavClick={this.handleNav} onBtnClick={this.handleNavClick} data={panels} flags={nav_status}/>
      </>
     );
  }
}
 
export default App;